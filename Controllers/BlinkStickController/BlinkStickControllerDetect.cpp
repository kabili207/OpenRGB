#include "Detector.h"
#include "BlinkStickController.h"
#include "RGBController.h"
#include "RGBController_BlinkStickNano.h"
#include "RGBController_BlinkStickSquare.h"
#include "RGBController_BlinkStickStrip.h"
#include <vector>
#include <regex>
#include <hidapi/hidapi.h>

#define BLINKSTICK_VID              0x20A0
#define BLINKSTICK_PID              0x41E5

#define BLICKSTICK_SQUARE_RELEASE   0x0200
#define BLICKSTICK_STRIP_RELEASE    0x0201
#define BLICKSTICK_NANO_RELEASE     0x0202
#define BLICKSTICK_FLEX_RELEASE     0x0203

/******************************************\
*                                          *
* BSnnnnnn-1.0                             *
* ||  |    | |- Software minor version     *
* ||  |    |--- Software major version     *
* ||  |-------- Denotes sequential number  *
* ||----------- Denotes BlinkStick device  *
*                                          *
\******************************************/
#define BLINKSTICK_PATTERN    L"BS\\d{6}-(\\d)\\.(\\d)"

/******************************************************************************************\
*                                                                                          *
*   DetectLogitechControllers                                                              *
*                                                                                          *
*       Tests the USB address to see if a Logitech RGB Keyboard controller exists there.   *
*                                                                                          *
\******************************************************************************************/

void DetectBlinkstickControllers(std::vector<RGBController*>& rgb_controllers)
{
    hid_init();
    hid_device_info* info = hid_enumerate(BLINKSTICK_VID, BLINKSTICK_PID);

    const std::wregex pattern(BLINKSTICK_PATTERN);

    while(info)
    {
        std::wcmatch version;
        if((info->vendor_id == BLINKSTICK_VID)
            &&(info->product_id == BLINKSTICK_PID)
            &&(std::regex_match(info->serial_number, version, pattern)))
        {
            hid_device* dev = hid_open_path(info->path);
            if (dev)
            {
                if (version[1] == L"1")
                {
                    // BlinkStick
                    // TODO: Version 1.2 requires color patch. Send flag to controller!!
                }
                else if (version[1] == L"2")
                {
                    // BlinkStick Pro
                }
                else if (version[1] == L"3")
                {
                    switch(info->release_number)
                    {
                        case BLICKSTICK_SQUARE_RELEASE:
                        {
                            // BlinkStick Square
                            BlinkStickController* controller = new BlinkStickController(dev, info->path);

                            RGBController_BlinkStickSquare* rgb_controller = new RGBController_BlinkStickSquare(controller);

                            rgb_controller->name = "BlinkStick Square";
                            rgb_controllers.push_back(rgb_controller);
                        }
                        break;
                        case BLICKSTICK_STRIP_RELEASE:
                        {
                            // BlinkStick Strip
                            BlinkStickController* controller = new BlinkStickController(dev, info->path);

                            RGBController_BlinkStickStrip* rgb_controller = new RGBController_BlinkStickStrip(controller);

                            rgb_controller->name = "BlinkStick Strip";
                            rgb_controllers.push_back(rgb_controller);
                        }
                        break;
                        case BLICKSTICK_NANO_RELEASE:
                        {
                            // BlinkStick Nano
                            BlinkStickController* controller = new BlinkStickController(dev, info->path);

                            RGBController_BlinkStickNano* rgb_controller = new RGBController_BlinkStickNano(controller);

                            rgb_controller->name = "BlinkStick Nano";
                            rgb_controllers.push_back(rgb_controller);
                        }
                        break;
                        case BLICKSTICK_FLEX_RELEASE:
                        {
                            // BlinkStick Flex
                        }
                        break;
                    }
                }
            }
        }
        info = info->next;
    }
    hid_free_enumeration(info);
}

REGISTER_DETECTOR("BlinkStick", DetectBlinkstickControllers);