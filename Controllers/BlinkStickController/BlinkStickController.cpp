/*-----------------------------------------*\
|  LogitechG810Controller.cpp               |
|                                           |
|  Driver for Logitech G810 Orion Spectrum  |
|  keyboard light controller                |
|                                           |
|  Adam Honse (CalcProgrammer1) 6/11/2020   |
\*-----------------------------------------*/

#include "BlinkStickController.h"
#include <cstring>
#include <cmath>
#include <chrono>
#include <thread>

BlinkStickController::BlinkStickController(hid_device* dev_handle, const char* path)
{
    dev = dev_handle;
    location = path;
}

BlinkStickController::~BlinkStickController()
{

}

std::string BlinkStickController::GetLocationString()
{
    return(location);
}

std::string BlinkStickController::GetSerialString()
{
    wchar_t serial[256];

    /*-----------------------------------------------------*\
    | Get the serial number string from HID                 |
    \*-----------------------------------------------------*/
    hid_get_serial_number_string(dev, serial, 256);

    /*-----------------------------------------------------*\
    | Convert wchar_t into std::wstring into std::string    |
    \*-----------------------------------------------------*/
    std::wstring wserial = std::wstring(serial);
    return std::string(wserial.begin(), wserial.end());
}

void BlinkStickController::SetLEDs(std::vector<RGBColor> colors)
{
    for (unsigned int idx = 0; idx < colors.size(); ++idx)
    {
        this->SetSingleLED(colors, idx);
    }
}

void BlinkStickController::SetSingleLED(std::vector<RGBColor> colors, int led)
{
    RGBColor color = colors[led];
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    this->SetColor(0, led, RGBGetRValue(color), RGBGetGValue(color), RGBGetBValue(color));
}

unsigned char BlinkStickController::GammaCorrect(unsigned char value) {
    
    return static_cast<unsigned char>(255.0f * pow(value / 255.0f, 1.0f / 0.45f) + 0.5f);
}

void BlinkStickController::SetColor
    (
    unsigned char       channel,
    unsigned char       index,
    unsigned char       red,
    unsigned char       green,
    unsigned char       blue
    )
{
    char usb_buf[6];

    red   = this->GammaCorrect(red);
    green = this->GammaCorrect(green);
    blue  = this->GammaCorrect(blue);

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    if (channel == 0 && index == 0)
    {
        usb_buf[0x00]           = 0x01;
        usb_buf[0x01]           = red;
        usb_buf[0x02]           = green;
        usb_buf[0x03]           = blue;
        hid_write(dev, (unsigned char *)usb_buf, 4);
    }
    else
    {
        usb_buf[0x00]           = 0x05;
        usb_buf[0x01]           = channel;
        usb_buf[0x02]           = index;
        usb_buf[0x03]           = red;
        usb_buf[0x04]           = green;
        usb_buf[0x05]           = blue;
        hid_write(dev, (unsigned char *)usb_buf, 6);
    }
}
