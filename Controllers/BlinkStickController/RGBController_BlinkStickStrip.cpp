/*-----------------------------------------*\
|  RGBController_BlinkStickSquare.cpp       |
|                                           |
|  Generic RGB Interface for Logitech G203  |
|  Prodigy RGB Mouse                        |
|                                           |
|  Amy Nagle (kabili207) 10/04/2020         |
\*-----------------------------------------*/

#include "RGBController_BlinkStickStrip.h"


RGBController_BlinkStickStrip::RGBController_BlinkStickStrip(BlinkStickController* blinkstick_ptr)
{
    blinkstick = blinkstick_ptr;

    name        = "BlinkStick Strip";
    type        = DEVICE_TYPE_LEDSTRIP;
    description = "BlinkStick Strip";
    location    = blinkstick->GetLocationString();
    serial      = blinkstick->GetSerialString();

    mode Direct;
    Direct.name       = "Direct";
    Direct.value      = 0;
    Direct.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Direct.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Direct);

    SetupZones();
}

void RGBController_BlinkStickStrip::SetupZones()
{
    zone led_zone;
    led_zone.name       = "LED Strip";
    led_zone.type       = ZONE_TYPE_LINEAR;
    led_zone.leds_min   = 8;
    led_zone.leds_max   = 8;
    led_zone.leds_count = 8;
    led_zone.matrix_map = NULL;
    zones.push_back(led_zone);

    for(int led_idx = 0; led_idx < 8; led_idx++)
    {
        led new_led;
        new_led.name = "LED ";
        new_led.name.append(std::to_string(led_idx));

        leds.push_back(new_led);
    }

    SetupColors();
}

void RGBController_BlinkStickStrip::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_BlinkStickStrip::DeviceUpdateLEDs()
{
    blinkstick->SetLEDs(colors);
}

void RGBController_BlinkStickStrip::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_BlinkStickStrip::UpdateSingleLED(int led)
{
    blinkstick->SetSingleLED(colors, led);
}

void RGBController_BlinkStickStrip::SetCustomMode()
{

}

void RGBController_BlinkStickStrip::DeviceUpdateMode()
{

}
