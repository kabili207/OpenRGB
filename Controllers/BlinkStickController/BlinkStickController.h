/*-----------------------------------------*\
|  LogitechG810Controller.h                 |
|                                           |
|  Definitions and types for Logitech G810  |
|  Orion Spectrum keyboard light controller |
|                                           |
|  Adam Honse (CalcProgrammer1) 6/11/2020   |
\*-----------------------------------------*/

#include "RGBController.h"

#include <string>
#include <hidapi/hidapi.h>

#pragma once

class BlinkStickController
{
public:
    BlinkStickController(hid_device* dev_handle, const char* path);
    ~BlinkStickController();

    std::string GetLocationString();
    std::string GetSerialString();
    void SetLEDs(std::vector<RGBColor> colors);
    void SetSingleLED(std::vector<RGBColor> colors, int led);

private:
    hid_device* dev;
    std::string             location;

    unsigned char GammaCorrect(unsigned char value);
    void SetColor
    (
    unsigned char       channel,
    unsigned char       index,
    unsigned char       red,
    unsigned char       green,
    unsigned char       blue
    );
};
