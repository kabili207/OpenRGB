/*-----------------------------------------*\
|  RGBController_BlinkStickSquare.cpp       |
|                                           |
|  Generic RGB Interface for Logitech G203  |
|  Prodigy RGB Mouse                        |
|                                           |
|  Amy Nagle (kabili207) 10/04/2020         |
\*-----------------------------------------*/

#include "RGBController_BlinkStickSquare.h"


//0xFFFFFFFF indicates an unused entry in matrix
#define NA  0xFFFFFFFF

static unsigned int matrix_map[4][4] =
    { { NA,   2,   3,  NA },
      {  1,  NA,  NA,   4 },
      {  0,  NA,  NA,   5 },
      { NA,   7,   6,  NA } };

static const char* zone_names[] =
{
    "LEDs",
};

static zone_type zone_types[] =
{
    ZONE_TYPE_MATRIX,
};

static const unsigned int zone_sizes[] =
{
    8,
};

RGBController_BlinkStickSquare::RGBController_BlinkStickSquare(BlinkStickController* blinkstick_ptr)
{
    blinkstick = blinkstick_ptr;

    name        = "BlinkStick Square";
    type        = DEVICE_TYPE_LEDSTRIP;
    description = "BlinkStick Square";
    location    = blinkstick->GetLocationString();
    serial      = blinkstick->GetSerialString();

    mode Direct;
    Direct.name       = "Direct";
    Direct.value      = 0;
    Direct.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Direct.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Direct);

    SetupZones();
}

RGBController_BlinkStickSquare::~RGBController_BlinkStickSquare()
{
    /*---------------------------------------------------------*\
    | Delete the matrix map                                     |
    \*---------------------------------------------------------*/
    for(unsigned int zone_index = 0; zone_index < zones.size(); zone_index++)
    {
        if(zones[zone_index].matrix_map != NULL)
        {
            delete zones[zone_index].matrix_map;
        }
    }
}

void RGBController_BlinkStickSquare::SetupZones()
{
    /*---------------------------------------------------------*\
    | Set up zones                                              |
    \*---------------------------------------------------------*/

    // The Square is sold either as a bare pcb (matrix) or
    // inside a large, opaque enclosure (single)
    #ifdef BLINKSTICK_SQUARE_MATRIX
    unsigned int total_led_count = 0;
    for(unsigned int zone_idx = 0; zone_idx < 1; zone_idx++)
    {
        zone new_zone;
        new_zone.name                   = zone_names[zone_idx];
        new_zone.type                   = zone_types[zone_idx];
        new_zone.leds_min               = zone_sizes[zone_idx];
        new_zone.leds_max               = zone_sizes[zone_idx];
        new_zone.leds_count             = zone_sizes[zone_idx];

        if(zone_types[zone_idx] == ZONE_TYPE_MATRIX)
        {
            new_zone.matrix_map         = new matrix_map_type;
            new_zone.matrix_map->height = 4;
            new_zone.matrix_map->width  = 4;
            new_zone.matrix_map->map    = (unsigned int *)&matrix_map;
        }
        else
        {
            new_zone.matrix_map         = NULL;
        }

        zones.push_back(new_zone);

        total_led_count += zone_sizes[zone_idx];
    }

    for(unsigned int led_idx = 0; led_idx < total_led_count; led_idx++)
    {
        led new_led;
        new_led.name = "LED ";
        new_led.name.append(std::to_string(led_idx));
        leds.push_back(new_led);
    }
    #else
    zone bottom_zone;
    bottom_zone.name       = "LED";
    bottom_zone.type       = ZONE_TYPE_SINGLE;
    bottom_zone.leds_min   = 1;
    bottom_zone.leds_max   = 1;
    bottom_zone.leds_count = 1;
    bottom_zone.matrix_map = NULL;
    zones.push_back(bottom_zone);

    led bot_led;
    bot_led.name = "LED";
    leds.push_back(bot_led);
    #endif

    SetupColors();
}

void RGBController_BlinkStickSquare::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_BlinkStickSquare::DeviceUpdateLEDs()
{
    #ifdef BLINKSTICK_SQUARE_MATRIX
    std::vector<RGBColor> c;
    for(unsigned int led_idx = 0; led_idx < 8; led_idx++)
        c.push_back(colors[0]);
    blinkstick->SetLEDs(c);
    #else
    blinkstick->SetLEDs(colors);
    #endif
}

void RGBController_BlinkStickSquare::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_BlinkStickSquare::UpdateSingleLED(int led)
{
    #ifdef BLINKSTICK_SQUARE_MATRIX
    blinkstick->SetSingleLED(colors, led);
    #else
    DeviceUpdateLEDs();
    #endif
}

void RGBController_BlinkStickSquare::SetCustomMode()
{

}

void RGBController_BlinkStickSquare::DeviceUpdateMode()
{

}
