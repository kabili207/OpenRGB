/*-----------------------------------------*\
|  RGBController_BlinkStickSquare.cpp       |
|                                           |
|  Generic RGB Interface for Logitech G203  |
|  Prodigy RGB Mouse                        |
|                                           |
|  Amy Nagle (kabili207) 10/04/2020         |
\*-----------------------------------------*/

#include "RGBController_BlinkStickNano.h"


RGBController_BlinkStickNano::RGBController_BlinkStickNano(BlinkStickController* blinkstick_ptr)
{
    blinkstick = blinkstick_ptr;

    name        = "BlinkStick Nano";
    type        = DEVICE_TYPE_LEDSTRIP;
    description = "BlinkStick Nano";
    location    = blinkstick->GetLocationString();
    serial      = blinkstick->GetSerialString();

    mode Direct;
    Direct.name       = "Direct";
    Direct.value      = 0;
    Direct.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Direct.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Direct);

    SetupZones();
}

void RGBController_BlinkStickNano::SetupZones()
{
    zone top_zone;
    top_zone.name       = "Top";
    top_zone.type       = ZONE_TYPE_SINGLE;
    top_zone.leds_min   = 1;
    top_zone.leds_max   = 1;
    top_zone.leds_count = 1;
    top_zone.matrix_map = NULL;
    zones.push_back(top_zone);

    led top_led;
    top_led.name = "Top";
    leds.push_back(top_led);

    zone bottom_zone;
    bottom_zone.name       = "Bottom";
    bottom_zone.type       = ZONE_TYPE_SINGLE;
    bottom_zone.leds_min   = 1;
    bottom_zone.leds_max   = 1;
    bottom_zone.leds_count = 1;
    bottom_zone.matrix_map = NULL;
    zones.push_back(bottom_zone);

    led bot_led;
    bot_led.name = "Bottom";
    leds.push_back(bot_led);

    SetupColors();
}

void RGBController_BlinkStickNano::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_BlinkStickNano::DeviceUpdateLEDs()
{
    blinkstick->SetLEDs(colors);
}

void RGBController_BlinkStickNano::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_BlinkStickNano::UpdateSingleLED(int led)
{
    blinkstick->SetSingleLED(colors, led);
}

void RGBController_BlinkStickNano::SetCustomMode()
{

}

void RGBController_BlinkStickNano::DeviceUpdateMode()
{

}
